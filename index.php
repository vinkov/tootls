<?php

require __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($loader, array(
    'cache' => __DIR__ . '/cache',
));

// create a login object. when this object is created, it will do all login/logout stuff automatically
// so this single line handles the entire login process. in consequence, you can simply ...
// $login = new Login();
$login = new vinkodlak\Tootls\Login();

// ... ask if we are logged in here:
if ($login->isUserLoggedIn() == false) {
    // the user is not logged in. 

    echo $twig->render('login.html', array(
        'alert' => $login
    ));

    exit;

} else {
    // the user is logged in.

    $action = new vinkodlak\Tootls\Action();

    echo $twig->render('index.html', array(
        'user' => $_SESSION['user_name'],
        'alert' => $action
    ));
    
}