<?php

namespace vinkoDlak\Tootls;

class Action
{
    public $errors = array();
    public $messages = array();
    public function __construct()
    {

        if (isset($_POST["download"])) {
            $this->doDownload();
        }
        elseif (isset($_POST["unzip"])) {
            $this->doUnzip();
        }
        elseif (isset($_POST["move"])) {
            $this->doMove();
        }
    }

    private function doDownload() {
        if (empty($_POST['input_field'])) {
            $this->errors[] = "Input field empty. Try again.";
        } else {

    		header('X-Accel-Buffering: no');
	        @ini_set('zlib.output_compression', 'Off');
    		@ini_set('output_buffering', 'Off');
   			@ini_set('output_handler', '');
       		if( function_exists('apache_setenv') ) // if this function is present, 
            	@apache_setenv('no-gzip', 1);    // then the .htaccess line can be omitted
    		// header('Content-Type: text/event-stream');
    		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
			//type octet-stream. make sure apache does not gzip this type, else it would get buffered
			header('Content-Type: text/octet-stream');
			header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

			$old_time = time();

			$curl = new \Curl\Curl();
			$curl->progress(function ($client, $download_size, $downloaded, $upload_size, $uploaded) {
			    if ($download_size === 0) {
			        return;
			    }

			    global $old_time;
			    $new_time = time();
	
				set_time_limit(30);

			    if ($new_time > $old_time) {
				    $progress = floor($downloaded * 100 / $download_size);
				    $old_time = $new_time;

				    $response = array('message' => $downloaded , 'progress' => $progress);
	     
				    echo json_encode($response) . PHP_EOL;
				    ob_flush();
				    flush();
			    }
			});
			$curl->complete(function ($instance) {
				$message = "Complete.";
				$response = array('message' => $message , 'progress' => 100);
			    echo json_encode($response) . PHP_EOL;
				set_time_limit(30);
			});
			// $curl->download('https://secure.php.net/distributions/manual/php_manual_en.html.gz', 'php_manual_en.html.gz');
			// $curl->download('http://ipv4.download.thinkbroadband.com/100MB.zip', 'php_manual_en.html.zip');
			$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
			$curl->setOpt(CURLOPT_TIMEOUT, 600);
			$curl->download($_POST['input_field'], basename($_POST['input_field']));

			
        	exit;
        }

    }

    private function doUnzip() {
        if (empty($_POST['input_field'])) {
            $this->errors[] = "Input field empty. Try again.";
        } else {
            $file = $_POST['input_field'];

            ignore_user_abort(true);
            set_time_limit(0);

            // get the absolute path to $file
            $path = pathinfo(realpath($file), PATHINFO_DIRNAME);

            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === TRUE) {
              // extract it to the path we determined above
              $zip->extractTo($path);
              $zip->close();
              $this->messages[] = "WOOT! $file extracted to $path";
            } else {
              $this->errors[] = "Doh! I couldn't open $file";
            }

            ignore_user_abort(false);
            set_time_limit(30);
        }
    }

    private function doMove() {
        if (empty($_POST['input_field'])) {
            $this->errors[] = "Input field empty. Try again.";
        } else {
            $source = realpath(dirname(__FILE__)) . "/". $_POST['input_field'];

            $cmd = 'mv "'. $source .'/*" "'. realpath(dirname(__FILE__)) .'/"'; 
            exec($cmd, $output, $return_val); 

            if ($return_val == 0) { 
                $this->messages[] = "Move success";
            } else { 
                $this->errors[] = "Move failed, ". $cmd;
                print_r($output);
            } 


            // $dest = pathinfo(realpath($file), PATHINFO_DIRNAME);

        }
    }


    // helper functions

    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file)
                if ($file != "." && $file != "..") rrmdir("$dir/$file");
            rmdir($dir);
        }
        else if (file_exists($dir)) unlink($dir);
    }

    private function rcopy($src, $dst, $delete_dst=false) {
        if ($delete_dst) {
            if (file_exists ( $dst ))
               rrmdir ( $dst );
        }
        if (is_dir ( $src )) {
            // mkdir ( $dst );
            $files = scandir ( $src );
            foreach ( $files as $file )
                if ($file != "." && $file != "..")
                    rcopy ( "$src/$file", "$dst/$file" );
        } else if (file_exists ( $src ))
            copy ( $src, $dst );
    }
}